﻿using System.Collections.Concurrent;
using System.Text;

namespace WordFinder {
    public class WordFinder {

        private readonly byte matrixWidth = 0;
        private readonly byte matrixHeight = 0;
        private readonly char[][] matrixChars;
        private const int wordsToReturn = 10;

        /// <summary>
        /// The WordFinder constructor receives a set of strings which represents a character matrix. The
        /// matrix size does not exceed 64x64, all strings contain the same number of characters.
        /// </summary>
        /// <param name="matrix"></param>
        public WordFinder(IEnumerable<string> matrix) {
            // It was not clarified in the challenge but given that "all strings contain the same number of characters"
            // I will assume each string in the collection is a row in the matrix. I will also assume this is a fact
            // and that I do not need to make sure all the strings have the same number of characters, which is something
            // I would normally do.

            // Starting with some parameter validation
            if (matrix == null || matrix.Count() == 0 || matrix.Count() > 64) {
                throw new ArgumentException("The matrix parameter must contain between one and sixty-four strings.", nameof(matrix));
            }
            if (String.IsNullOrEmpty(matrix.First()) || matrix.First().Length > 64) {
                throw new ArgumentException("Each string in the matrix must contain between one and sixty-four characters.", nameof(matrix));
            }

            // Now we turn the collection into an actual matrix
            matrixHeight = (byte)matrix.Count();
            matrixWidth = (byte)matrix.First().Length;

            matrixChars = new char[matrixHeight][];

            for (int i = 0; i < matrixHeight; i++) {
                // I will also assume this is meant to be case insensitive, so we will uppercase everything
                matrixChars[i] = matrix.ElementAt(i).ToUpper().ToCharArray();
            }
        }

        /// <summary>
        /// The "Find" method should return the top 10 most repeated words from the word stream found in the
        /// matrix. If no words are found, the "Find" method should return an empty set of strings.If any
        /// word in the word stream is found more than once within the stream, the search results
        /// should count it only once
        /// </summary>
        public IEnumerable<string> Find(IEnumerable<string> wordStream) {
            // Again, let's start by validating the parameters
            if (wordStream == null || wordStream.Count() == 0) {
                throw new ArgumentException("The word stream must contain at least one word in it.", nameof(wordStream));
            }

            // Change the collection to a hashset to remove duplicates and save iterations
            HashSet<string> words = new HashSet<string>(wordStream);

            // We will for now save the words found in a concurrent dictiornary
            ConcurrentDictionary<string, int> wordsFoundWithCount = new ConcurrentDictionary<string, int>();

            // We check for each word from the stream and add it to the dictionary
            Parallel.ForEach(words, word => {
                int timesFound = CountWordInMatrix(word.ToUpper());
                if (timesFound > 0) {
                    wordsFoundWithCount.AddOrUpdate(word, timesFound, (k, v) => timesFound);
                }
            });

            // Finally we sort the dictionary by value, cut the first ten, and return only the words
            IOrderedEnumerable<KeyValuePair<string, int>> sortedWords = from entry in wordsFoundWithCount orderby entry.Value descending select entry;
            return sortedWords.Take(wordsToReturn).Select(kv => kv.Key);
        }

        /// <summary>
        /// Search for the given word in the matrix and return how many times it was found
        /// </summary>
        private int CountWordInMatrix(string word) {
            int timesFound = 0;

            // First we will search horizontally
            for (int y = 0; y < matrixHeight; y++) {
                // But only until a position where the word can fit
                for (int x = 0; x <= matrixWidth - word.Length; x++) {
                    MatrixAtPositionContainsWord(x, y, 1, 0, word, ref timesFound);
                }
            }

            // Now we do the same but vertically, also considering the word has to fit so we don't
            // need to check all the rows
            for (int y = 0; y <= matrixHeight - word.Length; y++) {
                for (int x = 0; x < matrixWidth; x++) {
                    MatrixAtPositionContainsWord(x, y, 0, 1, word, ref timesFound);
                }
            }

            return timesFound;
        }

        /// <summary>
        /// This method is the one that actually validates if each character of the given word
        /// matches the characters at the given coordinates. Also receives if it should grow
        /// horizontally, vertically, or both.
        /// </summary>
        private void MatrixAtPositionContainsWord(int col, int row, byte colIncrease, byte rowIncrease, string word, ref int timesFound) {
            for (int i = 0; i < word.Length; i++) {
                if (matrixChars[row][col] != word[i]) {
                    return;
                }
                row += rowIncrease;
                col += colIncrease;
            }
            timesFound++;
        }

        /// <summary>
        /// A method I made for fun that returns a string representing the matrix so I can draw it on the console
        /// </summary>
        /// <returns>A string representing a table to clearly see the matrix</returns>
        internal string DrawMatrix() {
            StringBuilder stringBuilder = new StringBuilder();

            // Calculate the drawing size
            int drawWidth = matrixWidth * 2 + 1;
            int drawHeight = matrixHeight * 2 + 1;

            // Do some iteration to draw a pretty table
            for (int y = 0; y < drawHeight; y++) {
                if (y == 0) {
                    for (int x = 0; x < drawWidth; x++) {
                        if (x == 0) {
                            stringBuilder.Append('┌');
                        } else if (x == drawWidth - 1) {
                            stringBuilder.Append('┐');
                        } else if (x % 2 == 0) {
                            stringBuilder.Append('┬');
                        } else {
                            stringBuilder.Append('─');
                        }
                    }
                } else if (y == drawHeight -1) {
                    for (int x = 0; x < drawWidth; x++) {
                        if (x == 0) {
                            stringBuilder.Append('└');
                        } else if (x == drawWidth - 1) {
                            stringBuilder.Append('┘');
                        } else if (x % 2 == 0) {
                            stringBuilder.Append('┴');
                        } else {
                            stringBuilder.Append('─');
                        }
                    }
                } else if (y % 2 == 0) {
                    for (int x = 0; x < drawWidth; x++) {
                        if (x == 0) {
                            stringBuilder.Append('├');
                        } else if (x == drawWidth - 1) {
                            stringBuilder.Append('┤');
                        } else if (x % 2 == 0) {
                            stringBuilder.Append('┼');
                        } else {
                            stringBuilder.Append('─');
                        }
                    }
                } else {
                    for ( int x = 0; x < drawWidth; x++) {
                        if (x % 2 == 0) {
                            stringBuilder.Append('│');
                        } else {
                            stringBuilder.Append(matrixChars[y - (int)Math.Ceiling(y / 2m)][x - (int)Math.Ceiling(x / 2m)]);
                        }
                    }
                }
                stringBuilder.Append(Environment.NewLine);
            }

            return stringBuilder.ToString();
        }

    }
}
