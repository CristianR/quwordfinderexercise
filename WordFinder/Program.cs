﻿// Recieve and process the words that make up the matrix
List<string> matrix = new List<string>();
while (matrix.Count == 0) {
    Console.WriteLine("Insert the strings to make up the matrix, separated by space, then press enter:");
    string? input = Console.ReadLine();
    // Process the input
    // Make sure it's not empty
    if (String.IsNullOrWhiteSpace(input)) {
        continue;
    }
    matrix = input.Split(' ').ToList();

    // This should never be the case based on the previous check, but let's make sure we got at least one line
    if (matrix.Count == 0) {  continue; }

    // Let's also make sure all the lines have the same length and that it's greater than zero
    int lineLength = matrix.First().Length;
    if (lineLength == 0) {
        matrix = new List<string>();
        continue; 
    }
    bool lineLengthError = false;
    matrix.ForEach(line => {
        lineLengthError = line.Length != lineLength;
    });
    if (lineLengthError) { 
        matrix = new List<string>();
        Console.WriteLine("Please make sure all the inserted strings have the same amount of characters.");
        continue; 
    }
}
Console.WriteLine();

// Draw the received matrix on screen
Console.WriteLine("Matrix received:");
WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);
Console.WriteLine(wordFinder.DrawMatrix());
Console.WriteLine();

// Recieve and process the words to find in the matrix
List<string> wordStream = new List<string>();
while (wordStream.Count == 0) {
    Console.WriteLine("Now please input the words to look for inside the matrix, separated by space, then press enter:");
    string? input = Console.ReadLine();
    // Process the input
    // Make sure it's not empty
    if (String.IsNullOrWhiteSpace(input)) {
        continue;
    }
    wordStream = input.Split(' ').ToList();

    // This should never be the case based on the previous check, but let's make sure we got at least one line
    if (wordStream.Count == 0) { continue; }

    // Also clear any word that may be an empty string becasue of double spaces or something
    wordStream.RemoveAll(word => String.IsNullOrWhiteSpace(word));
}
Console.WriteLine();

List<string> wordsFound = wordFinder.Find(wordStream).ToList();
string wasWere = wordsFound.Count == 1 ? "was" : "were";
Console.WriteLine($"From the {wordStream.Count} given words, {wordsFound.Count} {wasWere} found in the matrix.");
if (wordsFound.Count > 0) {
    string thisThese = wordsFound.Count == 1 ? "This is" : "These are";
    Console.WriteLine($"{thisThese}:{Environment.NewLine}{String.Join(Environment.NewLine, wordsFound)}");
}
Console.WriteLine();

Console.WriteLine("Press any key to exit.");
Console.ReadKey();
