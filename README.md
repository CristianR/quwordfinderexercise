# Qu: WordFinder Exercise

## Introduction
The WordFinder exercise presents a challenge to design a class that efficiently searches a character matrix for a given set 
of words. This document outlines the problem, provides initial solutions labeled "The Human Way" and "The Computer Way," 
and discusses the considerations for system resource usage and performance.

## Description
I was requested to design a class for this exercise that would search a character matrix for a specific collection of words.
> Presented with a character matrix and a large stream of words, your task is to create a Class
that searches the matrix to look for the words from the word stream. Words may appear
horizontally, from left to right, or vertically, from top to bottom. 

The constructor of this class should take a collection of strings to set up the matrix, and the 
find function should take another collection of strings containing the words to search for inside the matrix.
> The WordFinder constructor receives a set of strings which represents a character matrix. The
matrix size does not exceed 64x64, all strings contain the same number of characters. The
"Find" method should return the top 10 most repeated words from the word stream found in the
matrix. If no words are found, the "Find" method should return an empty set of strings. If any
word in the word stream is found more than once within the stream, the search results
should count it only once.

Finally, I was instructed to consider system resource usage and performance.
> Due to the size of the word stream, the code should be implemented in a high performance
fashion both in terms of efficient algorithm and utilization of system resources. Where possible,
please include your analysis and evaluation.

## Addressing the issue
I came up with two potential solutions for this problem, which I will refer to as "The Human Way" and "The Computer Way". 
Both of these concepts' initial iterations are seen on the branches labeled `the-human-way` and `the-computer-way`, respectively.

### The human way
This method of word discovery in a matrix would be similar to how we, as humans, approach word puzzles. When we find the first 
character of a word, we check to see if any of the characters adjacent to it match the next character in the word. If any of 
them do, we repeat the process until the word is finished or the characters stops matching.

So I made the class constructor create and save a character matrix using the supplied strings so I could repeat this operation. 
The Find method would then only perform a series of iterations to compare each word received against each location in the 
matrix, determining whether each character in the word matches the subsequent character in the matrix.

#### Analysis
- **Algorithm Complexity**: The approach involves nested iterations, potentially resulting in higher time complexity.
- **Resource Utilization**: The matrix size is stored, utilizing memory proportional to the matrix's dimensions.

### The computer way
Although the technique for this alternative method of locating a word inside a matrix would be much simpler, not many 
individuals would be able to accomplish it, hence the moniker. In order to get the word we're looking for, the idea 
is to pre-calculate all the words that are created by the rows and columns of the matrix, then we check if the word we
are looking for is contained in any of them.

The development of this process was quite straightforward; I instructed the constructor to create a list with all the 
received strings and to compute and add the strings generated by subtracting the X character from each of the strings 
to be used as the columns to said list. Then, the find method's straightforward job is to repeatedly go through that 
list and see if the term we're looking for is present there.

#### Analysis
- **Reduced Complexity**: The approach reduces the need for nested iterations, potentially leading to lower time complexity.
- **Resource Considerations**: While the approach is efficient, it doubles the memory usage compared to "The human way", 
storing additional strings.

### Conclusion
After extensive testing and benchmarking, "The human way" was chosen despite the efficiency of "The computer Way". 
The decision was guided by the need to consider system resources. "The human way" uses less memory, storing a 64x64
character matrix, compared to "The computer way", which stores up to 128 strings of up to 64 characters each. 
The trade-off between algorithmic efficiency and resource utilization led to the selection of "The human way" 
for its balance between performance and memory usage.