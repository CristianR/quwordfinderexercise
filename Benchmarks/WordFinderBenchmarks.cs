﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmarks {

    [Config(typeof(AntiVirusFriendlyConfig))]
    [MemoryDiagnoser]
    public class WordFinderBenchmarks {

        private WordFinder.WordFinder wordFinder;
        private readonly List<string> matrix;
        private readonly List<string> wordStream;
        private readonly Consumer consumer = new Consumer();

        [Params(1000, 10000)]
        public int Runs;

        public WordFinderBenchmarks() {
            string loremIpsum = "Loremipsumdolorsitamet,consecteturadipiscingelit.Sedfringillamolestiesemper.Pellentesqueullamcorpertemporrisuseuimperdiet.Inacnibhvelfelisullamcorperfinibus.Proinportaaliquetorciutullamcorper.Proinsedenimturpis.Nullasitametconsequatsem.Etiamegetblanditenim.Nullamtincidunttinciduntante,etfacilisiseratornareut.Crasuttellusdolor.Duisfeugiatluctusmauris,acpretiumarculobortisquis.Donecurnaorci,vestibulumeulaoreetin,finibusinlorem.Proinnectempusex.Donecinsapienvitaerisusimperdietiaculiseuhendreritrisus.Donectortorenim,tinciduntquismimaximus,venenatisornareenim.Proineleifend,lacusquiscommodocommodo,lacusnisiscelerisqueodio,sedaliquamligulaligulaeunisi.Fuscesagittisnuncodio,apretiumodiopellentesqueeu.Suspendisseiaculisposueremalesuada.Praesentenimmagna,mattisvitaenullasitamet,tinciduntdictummagna.Donecquisorcietjustoelementumsagittis.Vestibuluminfinibusmetus.Phasellusegettortorvelexvolutpatmaximus.Fusceviverratellusvitaequammattis,nonsollicitudineratporttitor.Proininterdumjustoquisligulascelerisquevulputate.Fusceporttitor,semetviverracondimentum,metusdolorlaoreetlorem,sedlobortisjustoorcieuaugue.Maecenasquisduisedelitconsequattempus.Sedelementumultricieslectus.Vivamusaclacusveldiambibendumtemporpellentesquequispurus.Fusceegestaselitvelbibendumegestas.Nullarutrumelementumfinibus.Praesenttinciduntturpisvelanteelementumimperdietinaurna.Inlaoreetultriciesex,noneleifendduimollisid.Crasultriciesmaurisfringillanunccongue,sedcondimentumtortorcongue.Utaccumsantortoreunibhviverra,atfeugiatsemfacilisis.Nullamimperdietligulaenim,eufaucibusturpislaciniaeu.Maurispellentesqueturpisaugue,aultriciesexpellentesquesitamet.Vivamusconsecteturtinciduntrutrum.Phasellushendreritullamcorperelit,inauctornequeaccumsana.Pellentesquedapibuslacusorci,veltempusantefaucibuseget.Donecquisenimegetmiconsequatauctoraneclibero.Maecenasvehicula,antevitaefermentumhendrerit,orcisemfaucibusmetus,adictumerossemacmetus.Nuncetluctusdiam,vellaoreetjusto.Proineuismodvitaenullasedsuscipit.Fusceidnequeacestmaximushendreritetvitaeenim.Duiseuismodnibhlibero,egetegestasurnaconvallisnon.Nullamlobortisatvelitquisfacilisis.Utidfelisinenimrutrumaliquameuinmassa.Sedideuismodfelis.Curabiturnibhmauris,ullamcorpervitaeturpissed,aliquamdictumdiam.Nuncatligulasedorcilaciniatristiqueegetinleo.Pellentesquelaciniaerosquisrhoncustempus.Praesentvulputateposueresodales.Seddictumposueremattis.Etiameufringillasapien.Maurisnonjustoquam.Quisqueimperdietvestibulumante,sitametsuscipitlacusmaximusvel.Vestibulumfaucibusvariuseuismod.Maecenasaccumsanportaodio.Integertinciduntinnibhseddapibus.Sedeuturpisornare,consequatantevel,sagittisipsum.Inhachabitasseplateadictumst.Maurisvulputatetortoridvolutpatmollis.Nullamvitaeluctusleo.Aliquamligulaleo,maximusvelcommodoa,posuereneceros.Fuscetortoraugue,aliquamluctustellusac,bibendumplaceratfelis.Duisaliquamnuncsitametmagnaconsectetur,inmaximusmetusvehicula.Nunclobortiscondimentumnulla,euluctusnibhbibendumvitae.Praesentsempermagnasitameteuismodsagittis.Phasellusegetcommodoipsum.Nuncegetelitiderosefficiturvenenatisaegetest.Intemporarcuidmalesuadaornare.Praesentnonauctorpurus,necdictumodio.Duisvulputatequamjusto,atpellentesqueorcisagittisut.Crasidfacilisisrisus,eualiquetarcu.Suspendissecommodotristiqueante,sitametsuscipitnullarutrumut.Curabiturconsequatelitasagittismalesuada.Nullampretiumconvallisdui,sedsollicitudinextemporeu.Quisqueinviverramagna.Nullagravidacommodometusfringillainterdum.Maurisportasuscipitipsumatconvallis.Vivamusatlacusipsum.Maecenaserosnisi,pellentesquenecfacilisiseu,convallisvitaetellus.Sedlaciniaerosindiamcongue,atpellentesquemivehicula.Maecenasullamcorperleoutrisusmollisfeugiat.Morbietanteutsapientemporvehicula.Maurissollicitudinarcuegetelitscelerisqueauctor.Utultricesinduiettincidunt.Aliquamsempergravidatempor.Fuscetempor,miincondimentumpellentesque,justorisustempusmi,idefficiturligulaerosacnibh.Pellentesqueatmetussodales,viverraligulaeget,aliquetelit.Craserosfelis,ultricesamaurismollis,commodofermentumquam.Pellentesquehabitantmorbitristiquesenectusetnetusetmalesuadafamesacturpisegestas.Vivamustemporidnequequismolestie.Quisquedapibusimperdiet";
            matrix = new List<string>();
            for (int i = 0; i <= loremIpsum.Length - 64; i = i + 64) {
                matrix.Add(loremIpsum.Substring(i, 64));
            }
            wordStream = new List<string>() {
                "vest", "fuscesagi", "viva", "uameui", "nasullamcor", "aliqueta",
                "ecquisor", "noneleifendduimollisid", "sitametsuscipitlacusmaximusvel", "inmaximusmetusvehiula", "sedsollicitudinextemoreu", "lanunccongue,sedco",
                "ipsum.Maee", "etelitideroseffici", "gestaselitvel", "ornequeaccumsaan", "cquisorcietjust", "dnequeacestmaxmushen",
                "spurus.Fusce", "risusimperdie", "risusimperdie", "risusimperdie", "gestaselitvel", "uismodfelis",
                "uismodfelis", "eluctusl", "ntes", "eugiatlu", "acestmaximu", "bortiscondime",
                "bortiscondimer", "ugualms", "dremn", "amsi,", "beivas.pt", "beivas.ptr",
                "slvtldar", "darus", "unseevcocrsst", "loremipsum", "lebpurn", "oshrnsnee"
            };
            wordFinder = new WordFinder.WordFinder(wordStream);
        }

        [Benchmark]
        public void WordsFound() => (wordFinder.Find(wordStream)).Consume(consumer);

        [Benchmark]
        public void MultipleWordsFound() {
            for (int i = 0; i < Runs;  i++) {
                wordFinder.Find(wordStream);
            }
        }

        [Benchmark]
        public void MultipleInstancing() {
            for (int i = 0; i < Runs; i++) {
                WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);
            }
        }

        [Benchmark]
        public void MultipleInstancingAndFinding() {
            for (int i = 0; i < Runs; i++) {
                WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);
                wordFinder.Find(wordStream);
            }
        }

        [Benchmark]
        public void ParallelInstancingAndFinding() {
            Parallel.For(0, Runs, index => {
                WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);
                wordFinder.Find(wordStream);
            });
        }
    }
}
