using WordFinder;

namespace WordFinderTests {
    public class WordFinderTests {
        [Fact]
        public void WordFinder_BasicFindFromChallenge() {
            // Arrange
            List<string> matrix = new List<string>() {
                "abcdc", 
                "fgwio", 
                "chill", 
                "pqnsd", 
                "uvdxy" 
            };
            List<string> wordStream = new List<string>() { "cold", "wind", "snow", "chill" };
            WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);

            // Act
            IEnumerable<string> wordsFound = wordFinder.Find(wordStream);

            // Assert
            Assert.NotNull(wordsFound);
            Assert.Equal(3, wordsFound.Count());
            Assert.Contains("chill", wordsFound);
            Assert.Contains("cold", wordsFound);
            Assert.Contains("wind", wordsFound);
        }

        [Fact]
        public void WordFinder_BiggerMatrixAndWordStream() {
            // Arrange
            string loremIpsum = "Loremipsumdolorsitamet,consecteturadipiscingelit.Sedfringillamolestiesemper.Pellentesqueullamcorpertemporrisuseuimperdiet.Inacnibhvelfelisullamcorperfinibus.Proinportaaliquetorciutullamcorper.Proinsedenimturpis.Nullasitametconsequatsem.Etiamegetblanditenim.Nullamtincidunttinciduntante,etfacilisiseratornareut.Crasuttellusdolor.Duisfeugiatluctusmauris,acpretiumarculobortisquis.Donecurnaorci,vestibulumeulaoreetin,finibusinlorem.Proinnectempusex.Donecinsapienvitaerisusimperdietiaculiseuhendreritrisus.Donectortorenim,tinciduntquismimaximus,venenatisornareenim.Proineleifend,lacusquiscommodocommodo,lacusnisiscelerisqueodio,sedaliquamligulaligulaeunisi.Fuscesagittisnuncodio,apretiumodiopellentesqueeu.Suspendisseiaculisposueremalesuada.Praesentenimmagna,mattisvitaenullasitamet,tinciduntdictummagna.Donecquisorcietjustoelementumsagittis.Vestibuluminfinibusmetus.Phasellusegettortorvelexvolutpatmaximus.Fusceviverratellusvitaequammattis,nonsollicitudineratporttitor.Proininterdumjustoquisligulascelerisquevulputate.Fusceporttitor,semetviverracondimentum,metusdolorlaoreetlorem,sedlobortisjustoorcieuaugue.Maecenasquisduisedelitconsequattempus.Sedelementumultricieslectus.Vivamusaclacusveldiambibendumtemporpellentesquequispurus.Fusceegestaselitvelbibendumegestas.Nullarutrumelementumfinibus.Praesenttinciduntturpisvelanteelementumimperdietinaurna.Inlaoreetultriciesex,noneleifendduimollisid.Crasultriciesmaurisfringillanunccongue,sedcondimentumtortorcongue.Utaccumsantortoreunibhviverra,atfeugiatsemfacilisis.Nullamimperdietligulaenim,eufaucibusturpislaciniaeu.Maurispellentesqueturpisaugue,aultriciesexpellentesquesitamet.Vivamusconsecteturtinciduntrutrum.Phasellushendreritullamcorperelit,inauctornequeaccumsana.Pellentesquedapibuslacusorci,veltempusantefaucibuseget.Donecquisenimegetmiconsequatauctoraneclibero.Maecenasvehicula,antevitaefermentumhendrerit,orcisemfaucibusmetus,adictumerossemacmetus.Nuncetluctusdiam,vellaoreetjusto.Proineuismodvitaenullasedsuscipit.Fusceidnequeacestmaximushendreritetvitaeenim.Duiseuismodnibhlibero,egetegestasurnaconvallisnon.Nullamlobortisatvelitquisfacilisis.Utidfelisinenimrutrumaliquameuinmassa.Sedideuismodfelis.Curabiturnibhmauris,ullamcorpervitaeturpissed,aliquamdictumdiam.Nuncatligulasedorcilaciniatristiqueegetinleo.Pellentesquelaciniaerosquisrhoncustempus.Praesentvulputateposueresodales.Seddictumposueremattis.Etiameufringillasapien.Maurisnonjustoquam.Quisqueimperdietvestibulumante,sitametsuscipitlacusmaximusvel.Vestibulumfaucibusvariuseuismod.Maecenasaccumsanportaodio.Integertinciduntinnibhseddapibus.Sedeuturpisornare,consequatantevel,sagittisipsum.Inhachabitasseplateadictumst.Maurisvulputatetortoridvolutpatmollis.Nullamvitaeluctusleo.Aliquamligulaleo,maximusvelcommodoa,posuereneceros.Fuscetortoraugue,aliquamluctustellusac,bibendumplaceratfelis.Duisaliquamnuncsitametmagnaconsectetur,inmaximusmetusvehicula.Nunclobortiscondimentumnulla,euluctusnibhbibendumvitae.Praesentsempermagnasitameteuismodsagittis.Phasellusegetcommodoipsum.Nuncegetelitiderosefficiturvenenatisaegetest.Intemporarcuidmalesuadaornare.Praesentnonauctorpurus,necdictumodio.Duisvulputatequamjusto,atpellentesqueorcisagittisut.Crasidfacilisisrisus,eualiquetarcu.Suspendissecommodotristiqueante,sitametsuscipitnullarutrumut.Curabiturconsequatelitasagittismalesuada.Nullampretiumconvallisdui,sedsollicitudinextemporeu.Quisqueinviverramagna.Nullagravidacommodometusfringillainterdum.Maurisportasuscipitipsumatconvallis.Vivamusatlacusipsum.Maecenaserosnisi,pellentesquenecfacilisiseu,convallisvitaetellus.Sedlaciniaerosindiamcongue,atpellentesquemivehicula.Maecenasullamcorperleoutrisusmollisfeugiat.Morbietanteutsapientemporvehicula.Maurissollicitudinarcuegetelitscelerisqueauctor.Utultricesinduiettincidunt.Aliquamsempergravidatempor.Fuscetempor,miincondimentumpellentesque,justorisustempusmi,idefficiturligulaerosacnibh.Pellentesqueatmetussodales,viverraligulaeget,aliquetelit.Craserosfelis,ultricesamaurismollis,commodofermentumquam.Pellentesquehabitantmorbitristiquesenectusetnetusetmalesuadafamesacturpisegestas.Vivamustemporidnequequismolestie.Quisquedapibusimperdiet";
            List<string> matrix = new List<string>();
            for (int i = 0; i <= loremIpsum.Length - 64; i = i + 64) {
                matrix.Add(loremIpsum.Substring(i, 64));
            }
            List<string> wordStream = new List<string>() { 
                "vest", "fuscesagi", "viva", "uameui", "nasullamcor", "aliqueta",
                "ecquisor", "noneleifendduimollisid", "sitametsuscipitlacusmaximusvel", "inmaximusmetusvehiula", "sedsollicitudinextemoreu", "lanunccongue,sedco",
                "ipsum.Maee", "etelitideroseffici", "gestaselitvel", "ornequeaccumsaan", "cquisorcietjust", "dnequeacestmaxmushen",
                "spurus.Fusce", "risusimperdie", "risusimperdie", "risusimperdie", "gestaselitvel", "uismodfelis",
                "uismodfelis", "eluctusl", "ntes", "eugiatlu", "acestmaximu", "bortiscondime",
                "bortiscondimer", "ugualms", "dremn", "amsi,", "beivas.pt", "beivas.ptr",
                "slvtldar", "darus", "unseevcocrsst", "loremipsum", "lebpurn", "oshrnsnee"
            };
            WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);

            // Act
            IEnumerable<string> wordsFound = wordFinder.Find(wordStream);

            // Assert
            Assert.NotNull(wordsFound);
            Assert.Equal(10, wordsFound.Count());
            // Only these three show up every time because the rest is only found once and the sorting changes for some reason
            Assert.Contains("ntes", wordsFound);
            Assert.Contains("vest", wordsFound);
            Assert.Contains("viva", wordsFound);

        }

        [Fact]
        public void WordFinder_StringTooLongThrowsException() {
            // Arrange
            List<string> matrix = new List<string>() {
                "abcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdcabcdc"
            };

            // Act
            Action action = () => { WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix); };

            // Assert
            ArgumentException exception = Assert.Throws<ArgumentException>(action);
            Assert.Equal("Each string in the matrix must contain between one and sixty-four characters. (Parameter 'matrix')", exception.Message);
        }

        [Fact]
        public void WordFinder_StringTooShortThrowsException() {
            // Arrange
            List<string> matrix = new List<string>() {
                "", ""
            };

            // Act
            Action action = () => { WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix); };

            // Assert
            ArgumentException exception = Assert.Throws<ArgumentException>(action);
            Assert.Equal("Each string in the matrix must contain between one and sixty-four characters. (Parameter 'matrix')", exception.Message);
        }

        [Fact]
        public void WordFinder_EmptyMatrixThrowsException() {
            // Arrange
            List<string> matrix = new List<string>();

            // Act
            Action action = () => { WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix); };

            // Assert
            ArgumentException exception = Assert.Throws<ArgumentException>(action);
            Assert.Equal("The matrix parameter must contain between one and sixty-four strings. (Parameter 'matrix')", exception.Message);
        }

        [Fact]
        public void WordFinder_EmptyWordStreamThrowsException() {
            // Arrange
            List<string> matrix = new List<string>() {
                "abcdc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy"
            };
            List<string> wordStream = new List<string>() {};
            WordFinder.WordFinder wordFinder = new WordFinder.WordFinder(matrix);

            // Act
            Action action = () => { IEnumerable<string> wordsFound = wordFinder.Find(wordStream); };

            // Assert
            ArgumentException exception = Assert.Throws<ArgumentException>(action);
            Assert.Equal("The word stream must contain at least one word in it. (Parameter 'wordStream')", exception.Message);
        }
    }
}